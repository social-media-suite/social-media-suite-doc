---
title: Social media suite api
description: Rest API based in spring boot with mongo DB. UI based in Deno fresh
layout: base.njk
---

# Social media suite API

## Dev

### pre-hooks

Path `./.git/hooks/`

**pre-commit**

```
#!/bin/bash
./gradlew ktlintCheck
```

**pre-push**

```
#!/bin/bash
./gradlew clean test integrationTest jacocoTestReport
```

### Test Reports

- unit test `./build/reports/tests/test/index.html`
- integration test `./build/reports/tests/integrationTest/index.html`
- jacoco `./build/reports/jacoco/test/html/index.html`

![jacoco](/img/jacoco.png)

### SonarQube

- docker compose `docker-compose up -d`
- Persistent volume `./postgresql_data`
- Sonarqube command `./gradlew sonar`
- Sonarqube http://localhost:9000 credential admin / bitnami
- Sonarlint

![SonarQube using docker](/img/sonarqube.gif)

## Gitlab CICD

- linter `./gradlew ktlintCheck`
- unit test `./gradlew test`
- integration test `./gradlew integrationTest`
- jacoco `./gradlew jacocoTestReport`
- sonar `./gradlew sonar`
- build `./gradlew assemble`
- docker
- doc (only tag)
- deploy in okteto: `kubectl apply -f k8s`
- smoke test (status UP):
  https://social-media-suite-api-arturisimo.cloud.okteto.net/actuator/health

### Gitlab image repository

- login `docker login registry.gitlab.com`
- build
  `docker build -t registry.gitlab.com/social-media-suite/social-media-suite-api:latest .`
- tag
  `docker tag registry.gitlab.com/social-media-suite/social-media-suite-api:0.1.0`
- push
  `docker push registry.gitlab.com/social-media-suite/social-media-suite-api`

### Gitlab runner register

- check status `sudo gitlab-runner status`
- register `sudo gitlab-runner register`
- list configured runners `sudo gitlab-runner list`
- restart `sudo gitlab-runner restart`
- Docker-in-Docker requires privileged mode to function in
  `/etc/gitlab-runner/config.toml`

![gitlab runner](/img/gitlab_runner.gif)

### Gitlab pages

Publish in [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) static
websites from public folder using a job called "pages"

    deno task lume --dest=public

[https://social-media-suite-api-social-media-suite-0a6948b7e4856d33243f4.gitlab.io/](https://social-media-suite-api-social-media-suite-0a6948b7e4856d33243f4.gitlab.io/)

## Technologies

- Programming Language: [Kotlin](https://kotlinlang.org/)
- Build tool: [Gradle](https://gradle.org/)
- Relation Database: [PostgreSQL](https://www.postgresql.org/)
- Containers: [Docker](https://www.docker.com/)
- Container orchestration: [Kubernetes](https://kubernetes.io)
- PaaS: [Okteto](https://www.okteto.com)
- Application-framework: [Spring Boot](https://spring.io/projects/spring-boot)
- Monitoring
  [Spring Actuator](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html)
- JPA repositories:
  [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
- DB versioning: [Flyway](https://flywaydb.org/)
- JSON Serialization:
  [kotlinx.serialization](https://github.com/Kotlin/kotlinx.serialization)
- Api Documentation: [OpenAPi](https://swagger.io/specification/) /
  [springdoc-openapi](https://springdoc.org/)
- HTTP client: [Unirest-Java](http://kong.github.io/unirest-java/)
- Kotlin Linter: [ktlint-gradle](https://github.com/JLLeitschuh/ktlint-gradle)
- Test Mocking: [MockK](https://mockk.io/) /
  [SpringMockk](https://github.com/ninja-squad/springmockk)
- Integration test: [Testcontainers](https://testcontainers.com/)
- Test coverage: [JaCoCo](https://www.eclemma.org/jacoco/)
- Linter: [ktlint-gradle](https://github.com/JLLeitschuh/ktlint-gradle)
- Code quality: [SonarQube](https://www.sonarsource.com/products/sonarqube/)
- Static site generator [lume](https://lume.land/) for
  [Deno](https://deno.com/).
- Feature Flag [Unleash](https://docs.getunleash.io)


![UI](/img/ui.gif)

## Feature Flag in gitlab

Config feature flag [Unleash](https://docs.getunleash.io) in
[FeatureFlagConfig](https://gitlab.com/social-media-suite/social-media-suite-api/-/blob/main/src/main/kotlin/org/mastercloudapps/sms/config/FeatureFlagConfig.kt?ref_type=heads)

Commits:

- [upload image to imgbb.com disabled by FF](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/4562be70cdbddced07214937a2eaf9bc74888188)

- [remove FF](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/6013eaa11c5dd7764ee9440ea14d350c0c1c60bd)

![Feature flag](/img/gitlab_FF.gif)

## Integration with ImgBB.com

[ImgBB API](https://api.imgbb.com/) allows to upload pictures

```bash
curl --location --request POST "https://api.imgbb.com/1/upload?expiration=600&key=YOUR_CLIENT_API_KEY" --form "image=R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
```

## Deployment in okteto

[Okteto](https://www.okteto.com/) PaaS managed Kubernetes service designed for
remote development.

![Cluster in okteto](/img/okteto.gif)

# Database migration

## Branch by abstraction

- [abstraction for repository layer](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/50217d33b0ba0ca23954ec4011ae50ea1b3e69a0)
- [implementation mongodb repository](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/142666d5cd3c350510cfdbb46b3a537e7bca5b48)
- [feature flag to choose between repositories](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/4ef66cc8b316b2a9bc99cefa3a33b0e25bb3233a)
- [remove postgreSQL: remove FF, last stage branch by abstraction](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/b7e7636be5900f5785827cded0d54bb62b0a379f)

![featureFlag](/img/featureFlag.gif)

## Data migration

![data migration](/img/data_migration.png)
