---
title: Social media suite
description: Intro
layout: base.njk
---

![Master cloud apps](/img/LogoMCA.png)


# Social media suite

Aplicación web distribuida en front end y back end con despliegue continuo en un clúster de kubernetes empleando la
estrategía TBD con CI/CD. Esta aplicación es el trabajo fin de master  [CloudApps Master](https://www.codeurjc.es/mastercloudapps/) Desarrollo y despliegue de aplicaciones en la nube de la URJC

- [UI](https://social-media-suite-ui-arturisimo.cloud.okteto.net)
- [Memoria académica (ES)](https://gitlab.com/social-media-suite/social-media-suite-doc/-/blob/main/doc/memoria.pdf)
- [Documentación técnica (EN)](https://social-media-suite-doc-social-media-suite-b753d964522878b91d139.gitlab.io/)
- [Código](https://gitlab.com/social-media-suite)
- [API](https://social-media-suite-api-arturisimo.cloud.okteto.net/posts)
- [swagger](https://social-media-suite-api-arturisimo.cloud.okteto.net/swagger-ui/index.html)
- [API specs](https://social-media-suite-api-arturisimo.cloud.okteto.net/v3/api-docs)
