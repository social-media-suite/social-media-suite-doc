---
title: Social media suite api
description: Memoria del TFM
layout: base.njk
---

# Memoria

## Resumen

El trabajo fin de máster consiste en el desarrollo de una aplicación web distribuida en front end y back end con despliegue continuo en un clúster de kubernetes.

El desarrollo de la aplicación se ha realizado en varios hitos empleando la estrategia Trunk Based Development en un ciclo de desarrollo y entrega continua. 

## Objetivos

El objetivo principal es desarrollar un entorno de desarrollo en el que usando una única rama se puede desplegar en producción sin miedo a que el funcionamiento de la aplicación se vea afectado. Para este ejerccio se ha planteado el desarrollo de una aplicación capaz de publicar post con imágenes tomando como ejemplo pinterest.com en vario hitos.

Otros objetivos de este TFM serían:

 - Desarrollo de la aplicación usando Trunk Based Development y uso de las técnicas que se emplean con esta estratégia: implementación de Branch by abstraction y el uso de feature flags

 - Hacer uso de distintas herramientas de plataforma Gitlab: Gitlab CI, feature flags, pages, container registry.

 - Introducción a tecnologías fuera del ambito java: Deno, Deno Fresh, typescript, CSS tailwind, preact.

## Introducción

La aplicación consta de un API Rest que expone un único recurso "posts". El interfaz consume la API para funcionalidades básicas:

- Listado de posts
- Detalle de post
- Alta de un nuevo post
- Eliminación de un post

El desarrollo de la aplicación consta de una seria de hitos:

### Persistencia de posts

En este primer hito, sólo permite postear textos usando una base de datos relacional post PostgreSQL, para simular las imágenes se utilizaba un [placeholder](https://loremflickr.com/640/360).

![0.1.0](/img/tags/0.1.0.png)

### Añadir imágenes a los post

El segundo paso consiste en añadir imágenes a cada post. Para el almacenamiento de imágenes se utilizó [imgbb.com](https://imgbb.com/) que permite publicar imágenes mediante una sencilla [api](https://api.imgbb.com/). Una versión de la aplicación en esta fase se puede ver [aquí](https://social-media-suite-doc-social-media-suite-b753d964522878b91d139.gitlab.io/img/ui.gif).

![0.2.1](/img/tags/0.2.1.png)

### Migración de la base de datos a MongoDB

En esta fase se hizo una migración a una base de datos mongodb como ejercicio de las técnicas de branch by abstraction y feature flags. Ver [vídeo](https://social-media-suite-doc-social-media-suite-b753d964522878b91d139.gitlab.io/img/featureFlag.gif) de su uso.

![1.0.0](/img/tags/1.0.0.png)


## Tecnologías utilizadas

![CICD](/img/cicd.png)

## Desarrollo de la API

Para el desarrollo de api de posts se ha utilizado Spring Boot 3, utilizando kotlin 1.8.22. Además se ha utilizado Gradle como una herramienta de construcción y automatización para compilar y gestionar el proyecto del backend, ver [build.gradle.kts](https://gitlab.com/social-media-suite/social-media-suite-api/-/blob/main/build.gradle.kts).

### Arquitectura por capas 

La aplicación se descompone en capas, donde cada capa tiene una responsabilidad específica y se comunica solo con las capas adyacentes:

- Capa de presentación, paquete "rest" que atiende las peticiones del usuario
- Capa de lógica de negocio, paquete "services" que implementa la lógica de la aplicación
- Capa de persistencia, paquete "repository" que gestiona las operaciones de lectura y escritura en la base de datos

## Desarrollo del interfaz de usuario

Se ha utilizado Deno como entorno de ejecución creado por Ryan Dahl, el mismo desarrollador de Node.js. Alguna carácterísticas són:

- Seguro por defecto
- Soporte nativo para TypeScript y JSX.  
- Permite distribuir aplicaciones como un único archivo ejecutable
- Testeo, análisis estático, formateo incluidos por defecto
- No necesita npm, pero es compatible con versiones anteriores de Node.js y npm

Fresh es un framework web para deno, alguna carácterísticas serían:

- Por defecto, no se envía JavaScript al navegador, la mayor parte de la renderización de la página se realiza en el servidor, y en el cliente se renderizar pequeñas áreas de interactividad conocidas como islas
- El framework utiliza Preact (alternativa liviana a react) y JSX para la renderización y motor de plantillas.

## Documentación

Para la documentación se ha utilizado la herramienta [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) que publica sitios web estáticos basadas en el contenido del repositorio integración en el ciclo de integración continua. GitLab Pages es compatible varios generadores de sitios estáticos, en el marco del proyecto se ha utilizado [Lume](https://lume.land/) un generador de sitio estáticos para Deno.

Además, se ha realizado una página estática con un listado de commits, utilizando el API que ofrece GitLab desde un [script bash](https://gitlab.com/social-media-suite/social-media-suite-doc/-/blob/main/bin/generate_changeset.sh).

## Integración y entrega continua

Se ha definido un pipeline con una serie de etapas con distintas tareas que definen la construcción, prueba y despliegue de la aplicación. Los tareas (jobs) están definidos en el archivo [.gitlab-ci.yml](https://gitlab.com/social-media-suite/social-media-suite-api/-/blob/main/.gitlab-ci.yml) y son los siguientes:

![pipeline](/img/pipeline.png)

- Formateo de código `./gradlew ktlintCheck`
- Test unitarios `./gradlew test`
- Test de integración `./gradlew integrationTest`
- Análisis de covertura de test de jacoco `./gradlew jacocoTestReport`
- Construcción de el artefacto `./gradlew assemble`
- Construcción y publicación de la imágen docker
- Despligue en cluster de kubernetes: `kubectl apply -f k8s`
- Test de humo: `curl api/actuator/health`

En el caso de la aplicación UI el pipeline consta de las siguentes [etapas](https://gitlab.com/social-media-suite/social-media-suite-ui/-/blob/main/.gitlab-ci.yml)

- Formateo de código `deno task linter`
- Test unitarios `deno task test`
- Construcción y publicación de la imágen docker

Para la documentación el pipeline consta de un [único job](https://gitlab.com/social-media-suite/social-media-suite-doc/-/blob/main/.gitlab-ci.yml) que necesariamente se debe llamar "pages" para la publicación en gitlab pages con el contenido estático de la carpeta "public" 

- Generación de contenido estático `deno task lume --dest=../public`

**Ejecución de los jobs: Gitlab runner**

Para la ejecución de cada una los jobs, gitlab dispone de [GitLab Runner](https://docs.gitlab.com/runner/), puedes ejecutar usando un SaaS de pago. Como alternativa, se puede instalar en tu entorno local, registrar y configurar tus propios runners. En nuestro caso, se ha configurado que la ejecución se haga en ubuntu (WSL) usando contenedores docker.

![runner](/img/gitlab_runner.png)

**Publicación de las imágenes docker**

GitLab permite al desarrollador almacenar y gestionar imágenes de contenedores Docker en mismo entorno donde gestionan sus repositorios de código fuente. En el job de construcción de la imagen docker se "tagea" con el commit correspondiente y se publica en Gitlab container registry.

## Análisis estático de código

Se ha utilizado SonarQube para realizar análisis estático de código con el objetivo de mejorar la calidad del mismo. En un primer momento la idea era integrar el análisis en una etapa del pipeline alojando sonarqube en okteto. El problema es que ralentizaba mucho la ejecución por lo que se ha utilizado en el desarrollo local utilizando un [docker-compose](https://gitlab.com/social-media-suite/social-media-suite-api/-/blob/main/docker-compose.yml) como paso previo a casa commit del código.

![sonarqube](/img/sonarqube.png)

## Tests

Una parte importante de la estrategia TBD es contar con una serie de tests que nos permitan subir código a la rama main sin que se vea afectado el funcionamiento de la aplicación. Se ha contado con test unitarios y de integración en tareas separadas del ciclo CI:

### Test Unitarios

Spring Boot [SpringMockK](https://github.com/Ninja-Squad/springmockk) ofrece herramientas de testing como [MockK](https://mockk.io/) para la gestión de mocks para kotlin.

Para simular peticiones web sin ejecutar el servidor web, existe un cliente Http mock MockMvc que se conecta directamente al servidor web mock; ademñas se utilizan plugins de JUnit para controlar el ciclo de vida de estos elementos de forma muy sencilla.

### Test de integración con Testcontainer

Para contar con un entorno similar al de producción se ha utilizado la tecnología testcontainer para desplegar la base de datos de PostgreSQL o MongoDB; utilizando como cliente HTTP [Unirest](http://kong.github.io/unirest-java/).

![IT](/img/IT_report.png)

### Covertura de código con JaCoCo

Una etapa más del ciclo de integración continua es la comprobación que el umbral de covertura de test no baja de cierto porcentaje establecido en un 80%;

![JaCoCo](/img/jacoco.png)


## Despliegue en Okteto

[Okteto](https://www.okteto.com/) es una plataforma que facilita el desarrollo en la nube y el despliegue de aplicaciones en Kubernetes. Ofrece una capa gratuita para desplegar aplicacion en un cluster de kubernetes de, como máximo, 10 pods y 5Gb de almacenamiento. De manera automática, las aplicaciones inactivas se escalan automáticamente a cero después de 24 horas de inactividad.

Para el despliegue continuo se usa el [okteto manifest](https://gitlab.com/social-media-suite/social-media-suite-api/-/blob/main/okteto.yml), un archivo de configuración que define cómo se debe ejecutar y desarrollar una aplicación en Okteto. 

Okteto permite conectarse en remoto al cluste en el entorno de desarrollo. Para la gestión del cluster, ver logs, reiniciar pods... se ha utiliado K9s.

![Dashboard de okteto](/img/okteto.png)

## Trunk Based Development

Trunk-Based Development (TBD) es una estrategia de desarrollo de software que se centra en trabajar con una única rama principal (trunk) del repositorio de código. La idea principal es minimizar la complejidad y la cantidad de ramas en el sistema, promoviendo la integración continua y la entrega continua. En lugar de mantener varias ramas de desarrollo separadas, los desarrolladores trabajan directamente en la rama principal y realizan cambios pequeños y frecuentes.

Algunos aspectos clave de Trunk-Based Development son:

 * Commits Pequeños y Frecuentes, en lugar de trabajar en grandes cambios durante períodos prolongados. Cada commit debería representar un cambio atómico y funcional en el código.

* Uso de pre-hooks, scripts o comandos que se ejecutan antes de que se realice una acción específica, en nuestro caso de un commit o de un push. Ver anexo 2

* Integración Continua (CI): Cada commit desencadena automáticamente la ejecución de el pipeline para garantizar que el código siga siendo funcional y cumpla con los estándares de calidad.

* Feature Flags: uso de "toggles" para habilitar o deshabilitar características específicas en el código. Esto permite que las características estén presentes en el código base pero no sean visibles para los usuarios hasta que estén completamente desarrolladas y probadas.

* Branch by Abstraction En lugar de crear una rama específica para una nueva característica o cambio importante, se comienza introduciendo una abstracción (una interfaz). Esta abstracción inicializa la futura funcionalidad. Los cambios relacionados con la nueva funcionalidad se implementan gradualmente en el código principal, pero detrás de la abstracción. Esto permite que el código existente no se vea afectado directamente por los cambios, ya que la abstracción actúa como una capa intermedia 

* Revertir Rápidamente: Si un commit introduce un problema o error, TBD facilita la reversión rápida. Al revertir el commit problemático, se restaura rápidamente la estabilidad del código.

* Despliegue Continuo (CD): La entrega continua es un componente clave de TBD. La idea es que cualquier cambio que se integre correctamente en la rama principal esté listo para ser desplegado en producción.

* Evitar Ramas Prolongadas: A diferencia de otras estrategias que pueden involucrar ramas prolongadas para el desarrollo de características, TBD busca minimizar la existencia de ramas temporales para evitar problemas de integración posterior.

**Uso de feature flag**

Desde la versión 13.5, GitLab está integrado con [unleash](https://docs.getunleash.io) y proporciona tanto una interfaz de usuario para la gestión de feature flags, para todos los entornos o y usuarios específicos.

![UI para la gestion de ff en gitlab](/img/ff_gitlab.png)

Un ejemplo de uso de feature flags es la integración con la API de imgBB que en un primer momento se oculta hasta que el desarrollo ha finalizado:

- [Subida de imágenes a imgbb.com con el feature flag activado](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/4562be70cdbddced07214937a2eaf9bc74888188)
- Desarollo de la funcionalidad en distintos commits:  [fix upload to ImgBB + increase test coverage + doc](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/b5085b5de01f49a559d433a3b253894cee816297), [image with text mandatory](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/61a8cc5e92d0bff8ff63537fc25b0c04ded13d07), [fix env vars for FF](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/6cde446e87ea822508f30fbaed7b0577c799b726), [add thumb](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/60312fe52b273e2ac59941913b1d94cf1ff3e7c3), [fix upload file + redirect index](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/75f8a6796d6384ee06029cd94e5a3048a9fe3747)
- [Finalización: eliminar el FF](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/6013eaa11c5dd7764ee9440ea14d350c0c1c60bd)


**Branch by abstraction**

- [Abstracción de la capa de repository](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/50217d33b0ba0ca23954ec4011ae50ea1b3e69a0)
- [Implementación de el repository de mongodb](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/142666d5cd3c350510cfdbb46b3a537e7bca5b48)
- [Feature flag para escoger entre repositorios](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/4ef66cc8b316b2a9bc99cefa3a33b0e25bb3233a)
- [Eliminación de la persistencia con postgreSQL](https://gitlab.com/social-media-suite/social-media-suite-api/-/commit/b7e7636be5900f5785827cded0d54bb62b0a379f)

## Base de datos

Para la persistencia de los posts se ha optado por una base de datos PostgreSQL y para gestionar el versionado de la BBDD para cada uno de los hitos se ha utilizado Flyway.

Para la migración de los datos existentes antes de la migración a mongodb se hizo una inserción directamente en el pod de mongodb:

![Alta de posts](/img/data_migration.png)

## Trabajos futuros

Autentificación de usuarios, usando [Oauth2](https://datatracker.ietf.org/doc/html/rfc6749) con alguna plataforma que ofrece esta utilidad como Google, github o pinterest. Usando para ello la libreria [spring-boot-starter-oauth2-client](https://spring.io/guides/tutorials/spring-boot-oauth2/)

La integración con distintas redes sociales, como [pinterest](https://www.pinterest.es/) y [unsplash](https://unsplash.com/), para la consulta de post y publicación de post.

Hacer test de aceptación con [Selenium](https://www.selenium.dev/)

Alojamiento en una plataforma cloud como AWS

## Conclusiones


## Bibliografía y recursos utilizados

**Spring boot con kotlin**

- [Spring Time in Kotlin](https://www.youtube.com/playlist?list=PLlFc5cFwUnmxOJL0GSSZ1Vot4KL2Vwe7x)
- [Building web applications with Spring Boot and Kotlin](https://spring.io/guides/tutorials/spring-boot-kotlin/)


**Gitlab**

- [GitLab CI: Pipelines, CI/CD and DevOps for Beginners](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)
- [GitLab CI CD Pipeline Tutorial for Beginners](https://www.youtube.com/playlist?list=PLdsu0umqbb8PGnVWX3AV7zPI8VPRYtgOv)
- [Gitlab container registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
- [Feature flags in gitlab](https://medium.com/@supermegapotter/feature-flags-in-gitlab-7f7a0d53cfaa)
- [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Gitlab API](https://docs.gitlab.com/ee/api/rest/index.html)
- [Gitlab API Open API specs](https://gitlab.com/gitlab-org/gitlab/-/raw/93560c69d4c326b618ce633225c7c32358498391/doc/api/openapi/openapi.yaml)

**SonarQube**

- [Quality Assurance Using SonarQube: Gitlab & SonarQube Integration](https://medium.com/@alishayb/quality-assurance-using-sonarqube-gitlab-sonarqube-integration-f6ae61bc49f4)
- [Minimize technical debts by integrating SonarLint with SonarQube](https://ravitechverma.medium.com/minimize-technical-debts-by-integrating-sonarlint-with-sonarqube-da10ccea14ef)

**Trunk based development**

- [TDB](https://trunkbaseddevelopment.com)
- [Branch by Abstraction](https://trunkbaseddevelopment.com/branch-by-abstraction/)
- [Feature flags](https://trunkbaseddevelopment.com/feature-flags/)

**Deno**

- [Tutorial Desde Cero de DENO (Alternativa a Node.js)](https://www.youtube.com/watch?v=KsgDQL6jj2M)
- [Curso de Deno Fresh desde cero - ¡La alternativa de Next.js!](https://www.youtube.com/watch?v=t5TZB1KimfU)
- [Intro to islands](https://deno.com/blog/intro-to-islands)

## Proyectos preparatorios

- [Kotlin skeleton](https://gitlab.com/arturisimo/kotlin-skeleton/)
- [Introducción a Deno](https://gitlab.com/arturisimo/intro-fresh)

## Anexo

### Tecnologías utilizadas

**Back End**

- Lenguaje de programación:[Kotlin](https://kotlinlang.org/)
- Build tool: [Gradle](https://gradle.org/)
- Base de datos relacional [PostgreSQL](https://www.postgresql.org/)
- Base de datos noSQL [MongoDB](https://www.mongodb.com)
- FrameworkApplication-framework: [Spring Boot](https://spring.io/projects/spring-boot)
- Monitoring
  [Spring Actuator](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html)
- JPA repositories:
  [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
- Versionado de base de datos: [Flyway](https://flywaydb.org/)
- JSON Serialization:
  [kotlinx.serialization](https://github.com/Kotlin/kotlinx.serialization)
- Documentación de la API: [OpenAPi](https://swagger.io/specification/) /
  [springdoc-openapi](https://springdoc.org/)
- Cliente HTTP: [Unirest-Java](http://kong.github.io/unirest-java/)
- Test Mocking: [MockK](https://mockk.io/) /
  [SpringMockk](https://github.com/ninja-squad/springmockk)
- Test de integación: [Testcontainers](https://testcontainers.com/)
- Covertura de tests: [JaCoCo](https://www.eclemma.org/jacoco/)
- Linter: [ktlint-gradle](https://github.com/JLLeitschuh/ktlint-gradle)
- Análisis código: [SonarQube](https://www.sonarsource.com/products/sonarqube/)
- Feature Flag [Unleash](https://docs.getunleash.io)

**Front End**

- Lenguaje de programación: [Typescript](https://www.typescriptlang.org/)
- Entorno de ejecución de js: [Deno](https://deno.com/)
- Web framework: [Deno Fresh](https://fresh.deno.dev/)
- Framework de CSS: [Tailwind CSS](https://fresh.deno.dev/)

**CI/CD**

- Contenedores: [Docker](https://www.docker.com/)
- Orquestación de contenedores: [Kubernetes](https://kubernetes.io)
- Generador de sitios estáticos: [lume](https://lume.land/)
- Gestión cluster kubernete: [K9s](https://k9scli.io/)

**Plataformas externas**

- DevOps: [Gitlab](https://gitlab.com/)
- PaaS: [Okteto](https://www.okteto.com)
- Alojamiento de imágenes: [imgbb](https://es.imgbb.com/)

### Pre hooks

#### Back end

**pre-commit**

```
#!/bin/bash
./gradlew ktlintCheck
```

**pre-push**

```
#!/bin/bash
./gradlew clean test integrationTest jacocoTestReport
```

#### UI

```
#!/bin/bash
deno task check
deno task test
```